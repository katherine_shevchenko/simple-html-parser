import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AuthorParser extends Parser {
    private Author author;

    public AuthorParser(URL url) {
        super(url);
    }

    public Author getAuthor() {
        return author;
    }

    public static void main(String[] args) throws Exception {
        URL url = new URL("http://readly.ru/author/17138/");
        AuthorParser authorParser = new AuthorParser(url);
        authorParser.mainParseMethod();
        Author author = authorParser.getAuthor();
        System.out.println(author.toString());
    }

    @Override
    public void mainParseMethod() {
        // get information from html
        parseAllInformationAboutAuthor();
        // print all parsed information
        // or return book
    }

    private void parseAllInformationAboutAuthor() {
        author = new Author();
        author.setUrl(this.url.toString());
        // parse full name
        author.setFullName(parseAuthorFullName());
        // parse full name original
        author.setFullNameOrig(parseAuthorFullNameOriginal());
        // parse url of image
        author.setImageUrl(parseAuthorImage());
        // parse genres for this author
        author.setGenres(parseAuthorGenres());
    }

    private List<Genre> parseAuthorGenres() {
        List<Genre> genres = new ArrayList<>();

        String information = "жанры:";
        String result = findInformation(information, "<span class=\"row-value\">", "</span>");

        result = result.replaceAll("\n", "");
        result = result.replaceAll("\\s{3,}", "");
        result = result.replaceAll("span class=\"row-value\">", "");

        String[] arr = result.split(",");
        for (String str: arr) {
            String[] inf = str.split("\">");
            inf[0] = inf[0].replace("<a href=\"", ""); // url
            inf[1] = inf[1].replace("</a>", ""); // title

            try {
                Genre genre;
                // parse genre by GenreParser
                GenreParser genreParser = new GenreParser(new URL("http://readly.ru" + inf[0]));
                genreParser.mainParseMethod();
                // genre = result of work GenreParser
                genre = genreParser.getGenre();

                genres.add(genre);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return genres;
    }

    private String parseAuthorFullName() {
        String fullName;

        String information = "itemprop=\"name\"";
        String result = findInformation(information, ">", "<");
//        System.out.println("parseAuthorFullName: " + result);

        fullName = result;

        return fullName;
    }

    private String parseAuthorFullNameOriginal() {
        String fullNameOrig;

        String information = "class=\"author-name\"";
        String result = findInformation(information, ">", "<");
//        System.out.println("parseAuthorFullNameOriginal: " + result);

        fullNameOrig = result;

        return fullNameOrig;
    }

    private String parseAuthorImage() {
        String image;

        String information = "book-article--img";
        String result = findInformation(information, "<img src=\"", " alt=");
        result = result.replaceAll("img src=\"", "");
        result = result.replaceAll("\"", "");
//        System.out.println("parseAuthorImage: " + result);

        image = result;

        return image;
    }
}
