import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class BookParser extends Parser {
    private Book book;
    private List<Author> authors;
    private List<Genre> genres;
    private Serie serie;

    public BookParser(URL url) {
        super(url);
    }

    public Book getBook() {
        return book;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public Serie getSerie() {
        return serie;
    }

    @Override
    public void mainParseMethod() {
        // get information from html
        parseAllInformationAboutBook();
        // print all parsed information
        // or return book
    }

    private void parseAllInformationAboutBook() {
        book = new Book();
        // parse book title
        book.setTitle(parseBookTitle());
//        book.setUrl(this.url.toString().replaceAll("http://readly.ru", ""));
        book.setUrl(this.url.toString());
        // parse authors
        book.authors = parseAuthors();
        authors = book.authors;
        // parse year of book
        book.setYear(parseBookYear());
        // parse serie of book
        book.serie = parseBookSerie();
        serie = book.serie;
        // parse genres
        book.genres = parseGenres();
        genres = book.genres;
        // parse description
        book.setDescription(parseBookDescription());
        // parse rating
        book.setRating(parseBookRating());
        // parse image url
        book.setImageUrl(parseBookImage());
    }

    private String parseBookTitle() {
        String title;

        String information = "book-article--title";
        String result = findInformation(information, ">", "<");
//        System.out.println("Title: " + result);

        title = result;

        return title;
    }

    private List<Author> parseAuthors() {
        List<Author> authors = new ArrayList<>();

        String information = "Автор книги:";
        String result = findInformation(information, ">", "</div>");
        result = result.replaceAll("\n", "");
        result = result.replaceAll("\\s{3,}", "");

        String[] arr = result.split(",");
        for (String str: arr) {
            String[] inf = str.split("><");
            inf[0] = inf[0].replace("<a href=\"", "");
            inf[0] = inf[0].replace("\"", "");
            inf[1] = inf[1].replace("h2 itemprop=\"author\">", "");
            inf[1] = inf[1].replace("</h2", "");

            // inf[0] - url
            // inf[1] - fullName

//            System.out.println("Author: " + inf[1] + ", " + inf[0]);

            try {
                Author author;
                // parse author by AuthorParser
                AuthorParser authorParser = new AuthorParser(new URL("http://readly.ru" + inf[0]));
                authorParser.mainParseMethod();
                // author = result of work AuthorParser
                author = authorParser.getAuthor();

                authors.add(author);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return authors;
    }

    private int parseBookYear() {
        int year = 0;

        String information = "год написания:";
        String result = findInformation(information, "class=\"row-value\">", "<");
        result = result.replaceAll("lass=\"row-value\">", "");
//        System.out.println("Year: " + result);

        year = Integer.parseInt(result);

        return year;
    }

    private Serie parseBookSerie() {
        Serie serie;

        String information = "серия книг:";
        if (html.indexOf(information) == -1) {
            return null;
        }
        String result = findInformation(information, " href=\"", "</a>");
//        System.out.println("Serie: " + result);

        String [] arr = result.split(">");
        String url = arr[0];
        url = url.replaceAll("href=", "");
        url = url.replaceAll("\"", "");
        String title = arr[1];

        try {
            // parse serie by SerieParser
            SerieParser serieParser = new SerieParser(new URL("http://readly.ru" + url));
            serieParser.mainParseMethod();
            // serie = result of work SerieParser
            serie = serieParser.getSerie();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            serie = null;
        }

//        System.out.println(serie);

        return serie;
    }

    private List<Genre> parseGenres() {
        List<Genre> genres = new ArrayList<>();

        String information = "жанры:";
        String result = findInformation(information, "<span class=\"row-value\">", "</span>");

        result = result.replaceAll("\n", "");
        result = result.replaceAll("\\s{3,}", "");
        result = result.replaceAll("span class=\"row-value\">", "");
//        System.out.println("Genres: " + result);

        String[] arr = result.split(",");
        for (String str: arr) {
            String[] inf = str.split("\">");
            inf[0] = inf[0].replace("<a href=\"", "");
            inf[1] = inf[1].replace("</a>", "");

            // inf[1] - title
            // inf[0] - url

            try {
                Genre genre;
                // parse genre by GenreParser
                GenreParser genreParser = new GenreParser(new URL("http://readly.ru" + inf[0]));
                genreParser.mainParseMethod();
                // genre = result of work GenreParser
                genre = genreParser.getGenre();

                genres.add(genre);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

//        System.out.println(genres);

        return genres;
    }

    private String parseBookDescription() {
        String description;

        String information = "book-article--description";
        String result = findInformation(information, " <p>", "</div>");
        result = result.replaceAll("<p>", " ");
        result = result.replaceAll("</p>", " ");
        result = result.replaceAll("\n", " ");
        result = result.replaceAll("<a href=\"\\S*\">", "");
        result = result.replaceAll("</a>", "");
//        System.out.println("Description: " + result);

        description = result;

        return description;
    }

    private double parseBookRating() {
        double rating;

        String information = "book-profile--rate";
        String result = findInformation(information, ">", "<");
//        System.out.println("Rating: " + result);
        result = result.replaceAll(",", ".");

        rating = Double.parseDouble(result);

        return rating;
    }

    private String parseBookImage() {
        String imageUrl;

        String information = "book-article--img";
        String result = findInformation(information, "<img ", "alt");
        result = result.replaceAll("img ", "");
        result = result.replaceAll("src=\"", "");
        result = result.replaceAll("\"", "");
        result = result.replaceAll("\n", "");

//        System.out.println("IMAGE: " + result);

        imageUrl = result;

        return imageUrl;
    }
}