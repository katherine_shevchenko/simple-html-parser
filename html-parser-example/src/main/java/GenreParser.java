import java.net.URL;

public class GenreParser extends Parser {
    private Genre genre;

    public GenreParser(URL url) {
        super(url);
    }

    public Genre getGenre() {
        return genre;
    }

//    public static void main(String[] args) throws Exception {
//        URL url = new URL("http://readly.ru/genre/180/");
//        GenreParser genreParser = new GenreParser(url);
//        genreParser.mainParseMethod();
//        Genre genre = genreParser.getGenre();
//        System.out.println(genre.toString());
//    }

    @Override
    public void mainParseMethod() {
        // get information from html
        parseAllInformationAboutGenre();
        // print all parsed information
        // or return book
    }

    private void parseAllInformationAboutGenre() {
        genre = new Genre();
        genre.setUrl(this.url.toString());
        // parse title
        genre.setTitle(parseGenreTitle());
        // parse description
        genre.setDescription(parseGenreDescription());
    }

    private String parseGenreTitle() {
        String title;

        String information = "section-title-head";
        String result = findInformation(information, "><h1>", "</h1>");
        result = result.replaceAll("<h1>", "");
//        System.out.println("parseGenreTitle: " + result);

        title = result;

        return title;
    }

    private String parseGenreDescription() {
        String description;

        String information = "seo--genre-description";
        String result = findInformation(information, ">", "</p>");
        result = result.replaceAll("\\s{3,}", "");
        result = result.replaceAll("<a href=\"\\S*\">", "");
        result = result.replaceAll("</a>", "");
//        System.out.println("parseGenreDescription: " + result);

        description = result;

        return description;
    }
}
