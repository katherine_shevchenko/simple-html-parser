import java.util.List;

public class Author {
    private String fullName;
    private String fullNameOrig;
    private String url;
    private String imageUrl;
    // relationship with Genre Entity
    private List<Genre> genres;

    public Author() {}

//    public Author(String fullName, String url) {
//        this.fullName = fullName;
//        this.url = url;
//    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullNameOrig() {
        return fullNameOrig;
    }

    public void setFullNameOrig(String fullNameOrig) {
        this.fullNameOrig = fullNameOrig;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Author name: " + fullName
                + "\nAuthor name (original): " + fullNameOrig
                + "\nUrl: " + url
                + "\nImage: " + imageUrl
                + "\nGenres: " + genres;
    }
}