import java.util.List;

public class Book {
    private String title;
    private String url;
    private String imageUrl;
    private int year;
    private String description;
    private double rating;

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getYear() {
        return year;
    }

    public String getDescription() {
        return description;
    }

    public double getRating() {
        return rating;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    // relationships
    Serie serie;
    List<Author> authors;
    List<Genre> genres;

    @Override
    public String toString() {
        return "Title: \"" + title + "\""
                + "\nUrl: " + url
                + "\nImage: " + imageUrl
//                + "\nAuthor: " + authors
//                + "\nGenres: " + genres
//                + "\nSerie: " + serie
                + "\nYear: " + year
                + "\nRating: " + rating
                + "\nDescription: " + description;
    }
}