import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Parser {
    protected URL url;
    StringBuilder html;

    Parser(URL url) {
        this.url = url;
        html = new StringBuilder();

        // get html from url
        getHtmlFromUrl();
    }

    public void mainParseMethod() {
        // get information from html
        // print all parsed information
        // or return entity
    }

    private void getHtmlFromUrl() {
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                html.append(inputLine);
                html.append("\n");
            }
            in.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    String findInformation(String information, String targetStart, String targetEnd) {
        int start = html.indexOf(information);

        start = findPosition(start, targetStart);
        int end = findPosition(start, targetEnd);

        return html.substring(start + 1, end);
    }

    private int findPosition(int start, String target) {
        int positionResult = 0;

        int pos = 0;
        while(true) {
            int foundPos = html.indexOf(target, pos);
            if (foundPos == -1)
                break;

            if (foundPos > start) {
                positionResult = foundPos;
                break;
            }

            pos = foundPos + 1;
        }

        return positionResult;
    }
}