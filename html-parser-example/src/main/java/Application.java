import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws Exception {
        List<URL> urls = new ArrayList<>();
        urls.add(new URL("http://readly.ru/book/11994/"));
        urls.add(new URL("http://readly.ru/book/57703/"));
        urls.add(new URL("http://readly.ru/book/57641/"));
        urls.add(new URL("http://readly.ru/book/30537/"));
        urls.add(new URL("http://readly.ru/book/100576/"));

        for (URL url: urls) {
            System.out.println(url);
            BookParser bookParser = new BookParser(url);
            bookParser.mainParseMethod();

            Book book = bookParser.getBook();
            System.out.println(book);

            List<Author> authors = bookParser.getAuthors();
            authors.forEach(System.out::println);

            List<Genre> genres = bookParser.getGenres();
            genres.forEach(System.out::println);

            Serie serie = bookParser.getSerie();
            System.out.println(serie);
        }
    }
}