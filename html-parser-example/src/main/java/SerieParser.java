import java.net.URL;

public class SerieParser extends Parser {
    private Serie serie;

    public SerieParser(URL url) {
        super(url);
    }

    public Serie getSerie() {
        return serie;
    }

//    public static void main(String[] args) throws Exception {
//        URL url = new URL("http://readly.ru/serie/1378/");
//        SerieParser serieParser = new SerieParser(url);
//        serieParser.mainParseMethod();
//        Serie serie = serieParser.getSerie();
//        System.out.println(serie.toString());
//    }

    @Override
    public void mainParseMethod() {
        // get information from html
        parseAllInformationAboutSerie();
        // print all parsed information
        // or return book
    }

    private void parseAllInformationAboutSerie() {
        serie = new Serie();
        serie.setUrl(this.url.toString());
        // parse title
        serie.setTitle(parseSerieTitle());
        // parse description
        serie.setDescription(parseSerieDescription());
    }

    private String parseSerieTitle() {
        String title;

        String information = "series-detail-name";
        String result = findInformation(information, ">", "<");
        result = result.replaceAll("Серия ", "");
//        System.out.println("parseSerieTitle: " + result);

        title = result;

        return title;
    }

    private String parseSerieDescription() {
        String description;

        String information = "series-detail-description";
        String result = findInformation(information, " <p><p>", "</p></p>");
        result = result.replaceAll("<p>", "");
        result = result.replaceAll("</p>", "");
        result = result.replaceAll("\n", " ");
        result = result.replaceAll("\\s{3,}", "");
        result = result.replaceAll("<a href=\"\\S*\">", "");
        result = result.replaceAll("</a>", "");
//        System.out.println("parseSerieDescription: " + result);

        description = result;

        return description;
    }
}
